EXCLUDE_VERSIONS=3.14
include ${EPICS_ENV_PATH}/module.Makefile

DOC=doc
SOURCES=$(filter-out %.stt,$(wildcard src/*))
MISCS=$(wildcard protocol/*) $(wildcard misc/*)
